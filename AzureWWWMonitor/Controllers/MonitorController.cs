﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace AzureWWWMonitor.Controllers
{
    [Route("api/echo/")]
    public class LocalEchoController : Controller
    {
        private static readonly HttpClient client = new HttpClient();

        // GET api/echo/<monitorstring>?remote=remote
        [HttpGet("{id}")]
        public async Task<string> GetAsync(string id, [FromQuery]string remote)
        {
            if (remote == null || remote == "")
            {
                return id;
            }
            else
            {
                var request = new Uri("https://" + remote + "/api/echo/" + id);
                HttpResponseMessage response = await client.GetAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }
                return "error";
            }

        }
    }

    [Route("api/db/")]
    public class DBController : Controller
    {
        private static readonly HttpClient client = new HttpClient();

        // GET api/db/?remote=remote&dbserver=dbserver
        public async Task<string> GetAsync([FromQuery]string remote, [FromQuery]string dbserver)
        {
            if (remote != null && remote != "")
            {
                var request = new Uri("https://" + remote + "/api/db/?dbserver=" + dbserver);
                HttpResponseMessage response = await client.GetAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ToString();
                }
                return "error";
            }
            else
            {
                try
                {
                    string conn = "Server=tcp:" + dbserver + ",1433;Persist Security Info=False;User ID=monitor;Password=BigBrother2019!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;;";
                    
                    using (SqlConnection connection = new SqlConnection(conn))
                    {
                        connection.Open();
                        String sql = "SELECT TOP monitor_field FROM monitor";

                        using (SqlCommand command = new SqlCommand(sql, connection))
                        {
                            return (string)command.ExecuteScalar();
                        }
                    }
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (Exception e)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                    return "error";
                }
            }
        }
    }

}
